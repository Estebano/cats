import React, { useState, useEffect } from 'react';

const fetchAPI = (callback) => {
  fetch('http://agl-developer-test.azurewebsites.net/people.json')
      .then(response => response.json())
      .then(data => callback(data))
      .catch(error => alert(error))
};

const getCatsName = (owners) => {
 let  cats = [];
  owners.forEach(owner => {
    cats = [...cats, ...(owner.pets || []).filter(pet => pet.type === "Cat").map(cat => cat.name)];
  });
 return cats;
};


function App() {
  const defaultGender = {
    male: [],
    female: []
  };
  const [genders, setGenders] = useState(defaultGender);

  useEffect(() => {
    fetchAPI(data => {
      let males = data.filter(item => item.gender === 'Male');
      let females = data.filter(item => item.gender === 'Female');
      setGenders({
        male: getCatsName(males) ,
        female: getCatsName(females)
      })
    })
  }, []);

  return (
    <div className="container">
      {
        Object.entries(genders).map((gender, i) => {
          const [key, value] = gender;
          return (
              <div key={`${key}${i}`}>
                <h3>{key}</h3>
                <ul>
                  {
                    value.map((cat, y) => {
                      return(<li key={`${cat}${y}`}>{cat}</li>)
                    })
                  }
                </ul>
              </div>
          )
        })
      }
    </div>
  );
}

export default App;
